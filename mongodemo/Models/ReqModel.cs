﻿using System.Collections.Generic;

namespace mongodemo.Models
{
    public class ReqModel
    {
        public string sampleId { get; set; } = "";
        public string? title { get; set; }
        public string? desc { get; set; }
        public List<childmodel> detail { get; set; }
    }

    public class childmodel{
        public string title { get; set; } = "";
        public string desc { get; set; } = "";
    }
}
