﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using mongodemo.Models;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace mongodemo.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class cruidprocessController : ControllerBase
    {
        public string mongo = "mongodb+srv://automate:5844277072Cnt@cpxdbdemo.p0iip.mongodb.net/cpxmongo?w=majority";

        [HttpGet]
        public JsonResult getlist()
        {
            string res = "";
            string jsonpack = "";

            var json = JsonConvert.DeserializeObject("");
            MongoClient client = new MongoClient(mongo);
            try
            {
                var db = client.GetDatabase("cpxmongo");
                var coll = db.GetCollection<BsonDocument>("cruiddemo");
                var projection = Builders<BsonDocument>.Projection.Exclude("_id");
                var temp = coll.Find(new BsonDocument()).Project(projection).Sort("{title: 1}").ToList();
                long cnt = coll.Find(new BsonDocument()).Project(projection).Count();
                long i = 0;
                foreach (BsonDocument doc in temp)
                {
                    if (i == cnt - 1)
                    {
                        jsonpack += doc.ToString();
                    }
                    else
                    {
                        jsonpack += doc.ToString() + ",";
                    }
                    i++;
                }

                jsonpack = "\"count\": " + cnt + ",\"response\": [" + Regex.Unescape(jsonpack.ToString()) + "]";
            }
            catch (Exception e)
            {
                Response.StatusCode = 403;
                jsonpack = "\"msg\": \"" + e.Message.ToString() + "\"";
            }
            res = "{\"status\": " + Response.StatusCode + "," + jsonpack + "}";
            json = JsonConvert.DeserializeObject(res);
            return new JsonResult(json);
        }
        [HttpPost]
        public JsonResult insert([FromBody] ReqModel o)
        {
            o.sampleId = Guid.NewGuid().ToString();
            MongoClient client = new MongoClient(mongo);
            try
            {
                var db = client.GetDatabase("cpxmongo");
                var coll = db.GetCollection<BsonDocument>("cruiddemo");
                coll.InsertOne(o.ToBsonDocument());
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new JsonResult(new
                {
                    status = true,
                    message = "Found error: " + e.Message,
                });
            }
            return new JsonResult(new
            {
                status = true,
                message = "Insert success",
                recordId = o.sampleId
            });
        }
        [HttpPut]
        public JsonResult update([FromBody] ReqModel o)
        {
            MongoClient client = new MongoClient(mongo);
            try
            {
                var db = client.GetDatabase("cpxmongo");
                var coll = db.GetCollection<BsonDocument>("cruiddemo");
                var filter1 = Builders<BsonDocument>.Filter.Eq("sampleId", o.sampleId);
                coll.ReplaceOne(filter1, o.ToBsonDocument());
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new JsonResult(new
                {
                    status = true,
                    message = "Found error: " + e.Message,
                });
            }
            return new JsonResult(new
            {
                status = true,
                message = "Update record " + o.sampleId + " success"
            });
        }
        [HttpDelete]
        public JsonResult delete([FromBody] ReqModel o)
        {
            MongoClient client = new MongoClient(mongo);
            try
            {
                var db = client.GetDatabase("cpxmongo");
                var coll = db.GetCollection<BsonDocument>("cruiddemo");
                var filter1 = Builders<BsonDocument>.Filter.Eq("sampleId", o.sampleId);
                coll.DeleteOne(filter1);
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return new JsonResult(new
                {
                    status = true,
                    message = "Found error: " + e.Message,
                });
            }
            return new JsonResult(new
            {
                status = true,
                message = "Delete record " + o.sampleId + " success"
            });
        }
    }
}
